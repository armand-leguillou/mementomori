# Memento Mori Calendar

A calendar that shows you how long you have left to live, week by week.<br>
Your lifetime is estimate at 80 years. 4160 weeks.


### How to use it?
Copy the `mementomori-calendar-html` file in your computer and run it your browser.<br>
In the HTML file, replace the class "w" by "wp" (_week_ and _week passed_ abbreviations).<br>
The first sixteen years are already filled, but you can modify all.


### Demonstration
How it look in Joplin (note-taking app).
![](https://gitlab.com/armand-leguillou/mementomori/-/raw/main/Joplin%20screenshot.png?ref_type=heads)
